This is just a proof of concept to test the React Context API.

Basically it displays a select box which filters a table below.
The value of the filter (select) is not passed to the table as a prop, but as
a context.

## Available Scripts

In the project directory, you can run:

### `yarn start`

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
