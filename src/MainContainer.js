import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'
import InputLabel from '@material-ui/core/InputLabel'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'

import FilteredTable from './components/FilteredTable'
import { MyContext } from './MyContext'

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
})

class MainContainer extends Component {
  constructor (props) {
    super(props)

    this.handleChange = event => {
      this.setState({ [event.target.name]: event.target.value });
    }

    this.removeFilters = () => {
      this.setState({
        filterValue: 0
      })
    }

    this.state = {
      filterValue: 0,
      handleChange: this.handleChange,
      removeFilters: this.removeFilters
    }
  }

  render () {
    const { classes } = this.props

    return (
      <div style={{ margin: 10 }}>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="filter-simple">Max Calories</InputLabel>
          <Select
            value={this.state.filterValue}
            onChange={this.handleChange}
            inputProps={{
              name: 'filterValue',
              id: 'filter-simple',
            }}
          >
            <MenuItem value={0}>
              <em>No limit</em>
            </MenuItem>
            <MenuItem value={100}>One hundred</MenuItem>
            <MenuItem value={200}>Two hundred</MenuItem>
            <MenuItem value={300}>Three hundred</MenuItem>
          </Select>
        </FormControl>
        <MyContext.Provider value={this.state}>
          <FilteredTable />
        </MyContext.Provider>
      </div>
    )
  }
}

export default withStyles(styles)(MainContainer)
