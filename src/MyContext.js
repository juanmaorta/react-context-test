import React from 'react'

export const MyContext = React.createContext({
  filterValue: 0,
  handleChange: () => {},
  removeFilters: () => {}
});
