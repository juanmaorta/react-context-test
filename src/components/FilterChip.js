import React from 'react'
import Chip from '@material-ui/core/Chip'
import { withStyles } from '@material-ui/core/styles'

const styles = theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
  },
  chip: {
    margin: theme.spacing.unit,
  },
})

const FilterChip = (props) => {
  const { classes, filterValue, handleDelete } = props

  return (
    <div className={classes.root}>
      {filterValue !== 0 &&
        <Chip
          label={`Max calories: ${filterValue}`}
          className={classes.chip}
          color="secondary"
          onDelete={handleDelete}
        />
      }
    </div>
  )
}

export default withStyles(styles)(FilterChip)
