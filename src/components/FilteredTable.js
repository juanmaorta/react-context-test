import React from 'react'

import FilterChip from './FilterChip'
import SomeTable from './SomeTable'

import { MyContext } from '../MyContext'

const FilteredTable = () => (
  <div>
    <MyContext.Consumer>
    {({ filterValue, handleChange, removeFilters }) => {
      return (
        <div>
          <SomeTable
            filterValue={filterValue}
          />
          <FilterChip
            filterValue={filterValue}
            handleDelete={() => removeFilters()}
          />
        </div>
      )
    }}
    </MyContext.Consumer>
  </div>
)

export default FilteredTable
